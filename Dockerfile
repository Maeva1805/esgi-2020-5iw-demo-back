FROM composer:2.1 AS composer
WORKDIR /code/
COPY app/composer.json /code/
COPY app/composer.lock /code/
RUN composer install -o --no-scripts
FROM php:7.4-apache-buster
COPY ./app /var/www/html/
COPY --from=composer /code/vendor /var/www/html/vendor
WORKDIR /var/www/html/
RUN touch .env
RUN sed -ri -e 's!/var/www/html!/var/www/html/public!g' /etc/apache2/sites-available/*.conf
CMD sed -i "s/80/${PORT:-80}/g" /etc/apache2/sites-enabled/000-default.conf /etc/apache2/ports.conf && docker-php-entrypoint apache2-foreground
